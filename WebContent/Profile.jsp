<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="tag" %>
  
  <%@ page import="com.app.livebook.*"  %>
  
 <%
 	User user = (User) session.getAttribute("user");
 
 	if(user == null){
 		
 		response.sendRedirect("LogIn.jsp");
 		
 	}
 	
 
 %>
  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="./js/index.js" defer></script>
</head>
<body>



<form action="CreatePost" method="post">

<textarea rows="5" cols="10" name="postcontent">

</textarea>
<input type="file"  name="image">
<br>
 <button type="submit" name="Profile">Create Post</button>
 </form>
 <tag:forEach  var="post" items="${user.getPosts()}">
	<div class="post"  style="width:210px; height:300px ;border-width:2px ;border-style:solid"> 		
 		<form action="PostOperations">
 				<%-- <h1 class="content">${post.getContent()}<br></h1> --%>
 		
 				 <textarea rows="5" cols="10" name="content">${post.getContent()}</textarea>
 				
 				<%-- <div class="content" name="content"> ${post.getContent()} </div>--%>
 				
 				<h4>${post.getEmail()} <br></h4>
				<h4>${post.getDate()} <br></h4>
				<h4>${post.getPostId()} <br></h4>
 				<button type="submit" class="save"  name="save" value="${post.getPostId()}">Save</button>	
 		</form>
 		
 		<button  class="edit"  name="edit" onclick="edit(this)">Edit</button>
 		
 		<form action="PostOperations">
	 	<button name="delete" value="${post.getPostId()}">Delete</button>
	 	
 		</form>
 		
	</div>
 </tag:forEach>
 

</body>
</html>