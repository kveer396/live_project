<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="tag" %>
     
      <%@ page import="com.app.livebook.*"  %>
      
 <%
 	User currentuser = (User) session.getAttribute("user");
 
 	if(currentuser == null){
 		
 		response.sendRedirect("LogIn.jsp");
 		
 	}
 	
 	
 	
 	
 %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="Home" method="post">
	<h1>LiveBook</h1>
	<h1> Welcome <%=currentuser.getFname() +" "+ currentuser.getLname() %> </h1>
	<br>
	<div class="friendlist">
	<ul>
	<tag:forEach var="user" items="${allUsers}"></tag:forEach>
	<li><span>${user.getFname()}</span></li>
	
	</ul>
	 </div>
	
    <div style="float:right">
    <a style= "margin-right:10px" href="Profile">Profile</a>
    <a href="LogIn.jsp">Logout</a>
    </div>
    
    <br>
    
    <div style="top" id="search">
	<input type="text"  id ="myInput" placeholder="search" name="search" onkeyup="search(this)">
	</div>
    <tag:forEach var="post" items="${user.getPosts()}">
     <div class="profile" style="width:210px; height:250px ;border-width:2px ;border-style:solid">
		<div class="top">
		<label for="name">${post.getPostId()} <br></label>
			<label for="date" style="margin-left:50px">${post.getDate()} <br></label>
			
		</div>
		<br>
		<div class="middle">
			 <p>${post.getContent()} <p>
			 
		</div>
		<br>
		<div class="bottom">
			<label style= "margin-right:50px"><a href="url" >Like</a></label>
			<label><a href="url">Save</a></label>
		</div>
	</div>
   
    
    </tag:forEach>
    
    </form>
</body>
</html>