package com.app.servelet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.app.livebook.User;
import com.app.sql.PostDBUtil;
import com.app.sql.UserDBUtil;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private PostDBUtil postdb;
    private UserDBUtil userdb;
    ;
    
    @Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		
		try {
			postdb = new PostDBUtil(datasource);
			userdb = new UserDBUtil(datasource);
		}catch(Exception ex) {
			throw new ServletException(ex);
		}
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user =  (User) session.getAttribute("user");
		System.out.println(datasource+" datasource");
      
		ArrayList<User> friendlist = new ArrayList<>();
		try {
			friendlist.addAll(userdb.getAllUsers(datasource));
			System.out.println(friendlist.get(0).getFname());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 	
		try {
			postdb.getAllPost(user);
			
			session.setAttribute("user", user);
			session.setAttribute("allUsers", friendlist);
			 
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		response.sendRedirect("Home.jsp");
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
