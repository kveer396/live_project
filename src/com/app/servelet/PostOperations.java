package com.app.servelet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.app.livebook.User;
import com.app.sql.PostDBUtil;

/**
 * Servlet implementation class PostOperations
 */
@WebServlet("/PostOperations")
public class PostOperations extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostOperations() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private PostDBUtil postdb;
    ;
    
    
    @Override
   	public void init(ServletConfig config) throws ServletException {
   		// TODO Auto-generated method stub
   		super.init(config);
   		
   		try {
   			postdb = new PostDBUtil(datasource);
   		}catch(Exception ex) {
   			throw new ServletException(ex);
   		}
   	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		String delete = request.getParameter("delete");
		String edit = request.getParameter("edit");
		String save = request.getParameter("save");
		String content = request.getParameter("content");
		
		
		
		System.out.println(save);
		System.out.println(content);
	
		if(delete != null) {
			user.deletepost(delete, postdb);
			response.sendRedirect("Profile");
		}
		if(save != null) {
			user.savepost(save,postdb,content);
			response.sendRedirect("Profile");
			
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
