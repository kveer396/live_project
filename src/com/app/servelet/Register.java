package com.app.servelet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.app.livebook.User;
import com.app.sql.UserDBUtil;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }


	@Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDBUtil userdb;
    ;
    
    @Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		
		try {
			userdb = new UserDBUtil(datasource);
		}catch(Exception ex) {
			throw new ServletException(ex);
		}
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		
			
			HttpSession session = request.getSession();
			
			
			String Fname = request.getParameter("fname");
			String Lname = request.getParameter("lname");
			String Email = request.getParameter("email");
			String pass = request.getParameter("pass");
			
			User newuser = new User(Fname,Lname,Email,pass);
			
			boolean createduser = newuser.SignUp(userdb);
			
			if(createduser) {
				session.setAttribute("newuser", newuser);
				response.sendRedirect("Login.jsp");
			}else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("LogIn.jsp");
				request.setAttribute("registerError", true);
				dispatcher.forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
