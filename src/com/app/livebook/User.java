package com.app.livebook;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.app.sql.PostDBUtil;
import com.app.sql.UserDBUtil;

public class User {
	
	private String id;
	
	private String email;
	
	private String fname;
	
	private String lname;
	
	private String pass;
	
	private ArrayList<Post> posts = new ArrayList<>();
	private ArrayList<User> friends = new ArrayList<>();
	public User() {
		
	}
	public User(String fname, String lname, String email, String pass) {
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.pass = pass;
		
	}
	public User( String email, String pass) {

		this.email = email;
		this.pass = pass;
	}

	

	public boolean SignUp(UserDBUtil userdb) {
		
		try {
			userdb.Insert(this);
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
		
		
	}
	
	public ArrayList<User> searchuser(UserDBUtil userdb) {
		try {
			userdb.getAllUsers();
		}catch(Exception e) {
			e.getMessage();
		}
		return friends;
		
	}
	public boolean login(UserDBUtil db) {
		
		
		try {
			User founduser = db.findUser(this.email);
			System.out.println(founduser);
			if(founduser.getpass().equals(this.getpass()))
			{
				this.email = founduser.email;
				this.lname = founduser.lname;
				this.fname = founduser.fname;
				this.pass = founduser.pass;
				return true;
				
			}
		
		}
		catch(Exception e) {
			e.getMessage();
			
		}
		return false;
		
	}
	public void Createpost( String content,String image, PostDBUtil db) {
		
		try 
		{
			db.Insertpost(new Post(content,image,this.email));
		} 
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}
		
	}
	
	public void deletepost(String PostId ,PostDBUtil Postdb) {
		try {
			Postdb.deletepost(PostId);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void savepost(String edit, PostDBUtil Postdb,String content) {
		try {
			Postdb.savepost(edit, content);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void logout() {
		
	}
	
	public void SendMessage() {
		
	}
	
	
	public void viewprofile() {
		
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}
	
	public void setpass(String pass) {
		this.pass = pass;
	}
	
	public String getpass() {
		return pass;
	}
	public ArrayList<Post> getPosts() {
		return this.posts;
	}
	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}
	public ArrayList<User> getFriends() {
		return friends;
	}

	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}

	
}
