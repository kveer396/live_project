package com.app.livebook;

import java.sql.Timestamp;

import java.util.Date;
import java.util.Random;



public class Post {
	
	private String PostId;
	
	private String image;
	
	private String content;

	private Timestamp date;
	
	private String email;
	
	
	
	
	public Post(String postId, String email, String content, String image, Timestamp date) {

		this.PostId = postId;
		this.date  = date;
		this.content =content;
		this.image = image;
		this.email = email;
	}

	public Post(String content, String image,String email) {
		
		this.content = content;
		this.image = image;	
		this.PostId =  Integer.toString(new Random().nextInt(1000));
		this.date = new Timestamp(new Date().getTime());
		this.email = email;
		
	}
	public Post(String postId,String content) {
		this.PostId = postId;
		this.content = content;
		this.date =  new Timestamp(new Date().getTime());
	}

	public Post() {
		
	}

	
	public void EditPost() {
		
	}
	public void SavePost() {
		
	}
	
	
	
	public String getPostId() {
		return PostId;
	}
	public void setPostId(String postId) {
		PostId = postId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}


	public Timestamp getDate() {
		return date;
	}


	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	
	

}
