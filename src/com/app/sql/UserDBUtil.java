package com.app.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.app.livebook.User;

import java.sql.PreparedStatement;

public class UserDBUtil {

	
	private DataSource dataSource;
	
	public UserDBUtil (DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void Insert(User objuser) throws SQLException {
		System.out.println("insert");
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		try {
			conn = this.dataSource.getConnection();
			
			String sql = String.format("INSERT INTO user VALUES('%s','%s','%s','%s')",objuser.getFname(),objuser.getLname(),objuser.getEmail(),objuser.getpass());
			
			stm = conn.createStatement();
			
			stm.executeUpdate(sql);
			
			
			
		} finally {
			close(conn,stm,res);
		}
	}
	public User findUser(String email) throws Exception{
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		User foundUser = null;
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "SELECT * from user WHERE Email = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			res = pstmt.executeQuery();
			
			res.next();
			String Fname = res.getString("FirstName").toString();
			String Lname =res.getString("LastName").toString();
			String Email = res.getString("Email").toString();
			String Pass = res.getString("Password").toString();
			System.out.println(Fname);
			foundUser = new User(Fname,Lname,Email,Pass);
		
		}
		finally {
			close(conn,smt,res);
		}
		return foundUser;
	}
	
	public ArrayList<User> getAllUsers(DataSource dataSource) throws SQLException{
		
		
		
		System.out.println("in getall users");
		Connection conn = null;
		conn=dataSource.getConnection();
		ResultSet res = null;
		
		
		ArrayList<User> user = new ArrayList<>();
		try {
			conn = this.dataSource.getConnection();
			
			String sql = "SELECT * FROM  user";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			res = pstmt.executeQuery();
			while(res.next()) {
				
		   String fname = res.getString("FirstName").toString();
			String lname = res.getString("LastName").toString();
			String email1 = res.getString("Email").toString();
           User userResult=new User();
           userResult.setFname(fname);
           userResult.setLname(lname);
           userResult.setEmail(email1);
           user.add(userResult);
		
			}
			System.out.println(user+" user list");
			return user;
		}
		finally {
			
		}
		
	}
	
	private void close(Connection conn ,Statement smt , ResultSet res) {
		try {
			if(res != null) {
				res.close();
			}
			if(smt != null) {
				smt.close();
			}if(conn != null) {
				conn.close();
			}
		}catch(Exception exe) {
			exe.printStackTrace();
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
