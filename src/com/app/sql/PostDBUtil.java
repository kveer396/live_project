package com.app.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.app.livebook.User;
import com.app.livebook.Post;

public class PostDBUtil {

	
private DataSource dataSource;
	
	public PostDBUtil (DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	public void getUserPosts(User user) throws Exception{
		ArrayList<Post> posts = new ArrayList<>();
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
	
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "select * from posts WHERE email = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getEmail());
			res = pstmt.executeQuery();
			
			while(res.next()) {
				String postId = res.getString("postID").toString();
				String email = res.getString("email").toString();
				String content = res.getString("content").toString();
				String image = res.getString("image").toString();
				Timestamp date = res.getTimestamp("date");
			
				posts.add(new Post(postId,email,content,image,date));
			}
//			
			user.setPosts(posts);
		}
		finally {
			close(conn,res,smt);
		}
	}
	
	public void getAllPost(User user) throws  Exception {
		
//		ArrayList<Post> posts = new ArrayList<>();
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		ArrayList <Post> posts = new ArrayList<>();
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "select * FROM posts";
			smt = conn.createStatement();
			res = smt.executeQuery(sql);
			
			while(res.next()) {
				String PostId = res.getString("PostId").toString();
				String email = res.getString("email").toString();
				String content = res.getString("content").toString();
				String image = res.getString("image").toString();
				Timestamp date = res.getTimestamp("date");
				
				posts.add(new Post(PostId,email,content,image,date));
			}
//			
			user.setPosts(posts);
			
			
			}finally {
				close(conn,res,smt);
			}
		
	}
	
	public void Insertpost(Post objuser) throws SQLException {
		System.out.println("Create Post");
		
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		
		String postId = objuser.getPostId();
		String content = objuser.getContent();
		String image = objuser.getImage();
		String email = objuser.getEmail();
		Timestamp date = objuser.getDate();
		
		System.out.println(objuser.getPostId());
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "INSERT INTO posts VALUES(?,?,?,?,?)";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, postId);
			pstmt.setString(2, email);
			pstmt.setString(3, content);
			pstmt.setString(4, image);
			pstmt.setTimestamp(5, date);
			pstmt.executeUpdate();
			
		}
		finally {
			close(conn,res,smt);
		}
	}
	
	public void deletepost(String PostId)throws Exception{
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
			try {
				conn = this.dataSource.getConnection();
				String sql = "DELETE FROM posts WHERE postID=?";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, PostId);
				pstmt.executeUpdate();
			}finally {
				close(conn,res,smt);
			}
		}
	
	public void savepost(String postid,String content) throws SQLException{
		
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		
		
		System.out.println(postid);
		System.out.println(content);
	
		try {
			conn = this.dataSource.getConnection();
			String sql = "UPDATE posts SET content=? WHERE postID =?";
			
			
			System.out.println("hello2");
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(2, postid);
			pstmt.setString(1, content);
			int resu = pstmt.executeUpdate();
				
			System.out.println(resu);
			
			}finally {
				close(conn,res,smt);
				System.out.println("hello3");
			}
	}
	
	
	private void close(Connection conn ,ResultSet res , Statement smt) {
		try {
			if(res != null) {
				res.close();
			}
			if(smt != null) {
				smt.close();
			}if(conn != null) {
				conn.close();
			}
		}catch(Exception exe) {
			exe.printStackTrace();
		}
	}


	
}
